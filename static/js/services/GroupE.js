(function() {

  this.factory('GroupE', function(Restangular) {

  	attendanceData = {
    id: 1, 
    class_id: 1, 
    timestamp: "2022-11-13T10:39:35Z", 
    code: Math.floor(Math.random()*900) + 100,
    expiration_time: 600
	};

  	/*
       Return Record Based on all calls
	*/

    function getStats(){
      return Restangular.one('group_e/Stats/?status=1&timestamp=2014-07-13&class_id=1').get();
    }
    function getAttendance() {
      return Restangular.one('group_e/Attendance/?format=json&class_id=1').get();
    }



    function setAttendance(timestamp){
      console.log(timestamp);
      //check if there is one today, if there is update it other wise insert

      attendanceData.timestamp = timestamp;
      inData = Restangular.one('group_e/Attendance_Check/?timestamp='+timestamp+'&class_id=1').get();
      console.log(inData[6]);

      //if data exists just update row, other wise post
      if(!inData[0]){
         return Restangular.one('group_e/Attendance_Check/1/').customPUT(attendanceData);
      }
    	
    	return Restangular.one('group_e/Attendance_Check/').customPOST(attendanceData);

    }

    function changeStatus(id,studentId,classId,timeStamp,status){
        testData = {
        "id": id, 
        "student_id": studentId, 
        "class_id": classId, 
        "timestamp": timeStamp, 
        "status": status
      };
      return Restangular.one('group_e/Attendance/'+id+'/').customPUT(testData);
    }

    return {
      getAttendance: getAttendance, setAttendance: setAttendance, getStats: getStats, changeStatus:changeStatus
    };
  });

}).call(angular.module('services'));
