(function() {

  this.factory('Quiz', function($http){
      return {
          getQuiz: function(){
              return $http.get('/api/group_b/quiz/?format=json')
                  .then(function(result){
                      return result.data;
                  });
          },
//          removeQuiz: function(id){
//            return $http.delete('/api/group_b/quiz/'+id+'?format=json')
//          }
          removeQuiz: function (quiz) {
                    var id = quiz[0];
                    return $http.delete('/api/group_b/quiz/'+id+'?format=json');
                }
      }
  });



  this.factory('GroupB', function(Restangular) {

    function testCall() {
      return Restangular.one('endpoint.json').get();
    }

    return {
      testCall: testCall
    };
  });

}).call(angular.module('services'));
