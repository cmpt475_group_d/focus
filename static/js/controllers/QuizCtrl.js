(function() {

  this.controller('GroupBCtrl', function($scope, GroupB) {
    console.log('GroupBCtrl');
    $scope.hello = 'Hello from GroupBCtrl';

    GroupB.testCall().then(function(data) {
      if (data.test === "success") {
        $scope.testCall = true;
      }
    });
  });


  this.controller('QuizCtrl', function($scope, $location, Quiz){

    Quiz.getQuiz().then(function(quiz_data){
        $scope.quizzes = quiz_data;
        $scope.quizList = []
        for (i in $scope.quizzes){

//            $scope.quiz = [
//                {
//                    'id': angular.fromJson($scope.quizzes[i]).id,
//                    'name': angular.fromJson($scope.quizzes[i]).quizName,
//                    'quizWorth': angular.fromJson($scope.quizzes[i]).quizWorth,
//                    'update': angular.fromJson($scope.quizzes[i]).update,
//                    'timestamp': angular.fromJson($scope.quizzes[i]).timestamp,
//                    'creator': angular.fromJson($scope.quizzes[i]).creator
//                }];
            $scope.quiz = []
            $scope.id = angular.fromJson($scope.quizzes[i]).id;
            $scope.name = angular.fromJson($scope.quizzes[i]).quizName;
            $scope.quizWorth = angular.fromJson($scope.quizzes[i]).quizWorth;
            $scope.update = angular.fromJson($scope.quizzes[i]).updated;
            $scope.timestamp = angular.fromJson($scope.quizzes[i]).timestamp;
            $scope.creator = angular.fromJson($scope.quizzes[i]).creator;

            $scope.quiz.push($scope.id);
            $scope.quiz.push($scope.name);
            $scope.quiz.push($scope.quizWorth);
            $scope.quiz.push($scope.update);
            $scope.quiz.push($scope.timestamp);
            $scope.quiz.push($scope.creator);
            $scope.quizList.push($scope.quiz);

            console.log($scope.id);
            console.log($scope.name);
            console.log($scope.timestamp);
            console.log($scope.creator);
            console.log($scope.quiz);
            console.log($scope.quizList);
        }
    })
//      $scope.remove = function (quiz) {
//            quiz.remove()
//                .success(function (data) {
//                    alert("Deleted "+quiz[1]+"Successfully!!");
//                    $.each($scope.quizzes, function (id) {
//                        if ($scope.quizzes[i][0] === quiz[0]) {
//                            $scope.quizzes.splice(id, 1);
//                            return false;
//                        }
//                    }).fail(function(data) {
//                   alert("An Error has occured while deleting quiz!");
//                })
//        })};
      $scope.remove = function(){
          var current_quiz = this.quiz
          console.log(current_quiz);
          Quiz.removeQuiz(current_quiz)
              .success(function (data) {
                        alert("Deleted "+current_quiz[1]+"Successfully!!");
                        window.location.reload();
                        $.each($scope.quizzes, function (id) {
                            if ($scope.quizzes[i][0] === current_quiz[0]) {
                                $scope.quizzes.splice(id, 1);
                                return false;
                            }
                        });
                        //$scope.loading = false;
                    })
              .error(function (data) {
                        $scope.error = "An Error has occured while deleting quiz!" ;
                        //$scope.loading = false;
                    });
      }
  });


  this.controller('DetailCtrl', function($scope, $routeParams, Quiz) {
          Quiz.getQuiz().then(function(quizzes){
              $scope.id = parseInt($routeParams.idx);
              for (var i = 0; i < quizzes.length; i++) {
                  if ($scope.id === quizzes[i].id) {
                        $scope.item = quizzes[i];
                  }
              }

              console.log(quizzes);
              console.log($scope.id);
          });
  });


     this.directive('bars', function ($parse) {
          return {
             restrict: 'E',
             replace: true,
             template: '<div id="chart"></div>',
             link: function (scope, element, attrs) {
               var data = attrs.data.split(','),
               chart = d3.select('#chart')
                 .append("div").attr("class", "chart")
                 .selectAll('div')
                 .data(data).enter()
                 .append("div")
                 .transition().ease("elastic")
                 .style("width", function(d) { return d + "%"; })
                 .text(function(d) { return d + "%"; });
             }
          };
     });
}).call(angular.module('controllers'));
