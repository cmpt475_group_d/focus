(function() {


  this.controller('GroupCCtrl', function($scope, GroupC) {
    //console.log('GroupCCtrl');
    //$scope.hello = 'Hello from GroupCCtrl';

    GroupC.testCall().then(function(data) {
      if (data.test === "success") {
        $scope.testCall = true;
      }
    });
  });

  this.controller('questionFormCtrl', function ($scope, $timeout, toaster, $http) {
    $scope.tab = 'addQuestion';
    $scope.questions = [];
    $scope.answers = [ {right:false, ans: ''}, {right:false, ans: ''} ];
    $scope.retrievedQuestions = [];
    $scope.displayedQuestions = [];

    // Function for toast notifications
    $scope.pop = function(type, title, body, timeout, bodyOutputType, clickHandler){
            toaster.pop(type, title, body, timeout, bodyOutputType, clickHandler);
    };


	   // View questions
    $scope.v_removeQuestion = function(question) {
      var index = $scope.displayedQuestions.indexOf(question);
      $scope.displayedQuestions.splice(index, 1);
      $scope.alteredQuestions = true;
      $scope.errors = ''
      $http.get('/api/group_c/question/remove/' + question.id).success( function () {
        //console.log(data);
        console.log("http request to " + '/api/group_c/question/remove/' + question.id + " succeed");
        });
        $http.get('/api/group_c/data/get/').success( function (data) {
        console.log(data);
        console.log("http request to " + '/api/group_c/data/get/' + " succeed");
        $scope.retrievedQuestions = data;
      });
      $location.refresh();
    };

	$scope.editQuestion = function(question) {
		console.log("Editing question");
		$scope.editableQuestion = angular.copy(question);
		console.log("Finished setting up edit");
	};

	$scope.cancelEditQuestion = function() {
		console.log("Cancelling edit");
		$scope.editableQuestion = null;
		console.log("Cancelled edit");
	};

	$scope.submitEditQuestion = function(question) {
		console.log("Submitting edit");
		if (question.title == undefined || question.title == '') {
			$scope.flag = 1;
			$scope.errors = 'Please enter a question title';
			$timeout(function () {
			  $scope.flag = 0;
			}, 3000);
		} else {
			// update the question title
			url = '/api/group_c/question/modify/' + $scope.editableQuestion.id + "/" + $scope.editableQuestion.title;
			$http.get(url).success(function (data) {
				//console.log(data);
				console.log("Question title successfully updated");
			});
			
			// update the answer contents
			/*
			for (j = 0; j < $scope.editableQuestion.answer.length; j++)
				url = '/api/group_c/answer/modify/' + $scope.editableQuestion.id + '/' + $scope.editableQuestion.answer[j].number + '/' + $scope.editableQuestion.answer[j].ans;
			console.log("Url to submit edit data: " + url);
			
			$http.get(url).success(function (data) {
				//console.log(data);
				console.log("Answers successfully updated");
			});
			*/
		}
		
		$scope.editableQuestion = null;
		console.log("Submitted edit");
	};
    
    $scope.saveQuestions = function($event) {
      $scope.retrievedQuestions = [];
      for (i = 0; i < $scope.displayedQuestions.length; i++) { 
        var newQuestion = angular.copy($scope.displayedQuestions[i]);
        $scope.retrievedQuestions.push(newQuestion);
      }
        $scope.alteredQuestions = false;
        $event.preventDefault();
        // api call here to write to db
  
    };
    
    $scope.resetQuestions = function($event) {
      $scope.displayedQuestions = [];
      for (i = 0; i < $scope.retrievedQuestions.length; i++) { 
        var newQuestion = angular.copy($scope.retrievedQuestions[i]);
        $scope.displayedQuestions.push(newQuestion);
      }
      $scope.alteredQuestions = false;
      $event.preventDefault();
    };
    
    
    $scope.activateQuestion = function(question) {
    	question.activated = true;
        $http.get('/api/group_c/question/activate/' + question.id).success( function () {
          console.log(data);
          console.log("http request to " + '/api/group_c/data/get/' + " succeed");
        });
    	// api call to activate question
    };
    
    $scope.deactivateQuestion = function(question) {
    	question.activated = false;
        $http.get('/api/group_c/question/deactivate/' + question.id).success( function () {
          console.log(data);
          console.log("http request to " + '/api/group_c/question/deactivate/' + question.id + " succeed");
        });
    	//api call to activate question
    };

    //  Add Question is used for adding questions to the list of ALL questions to be submitted.
    //  Should not be used in any backend code whatsoever. FRONT-END ONLY
    $scope.addQuestion = function() {
      var questionTitle = $scope.questionTitle;
      var answers = $scope.answers;
      if (questionTitle != '' && questionTitle != undefined) {
        var newQuestion = { title:questionTitle, answer:answers, activated:false};
        $scope.questions.push(newQuestion);
        $scope.questionTitle = '';
        $scope.answers = [ {right:false, ans: ''}, {right:false, ans: ''} ];
        $scope.errors = '';
      //   $http.get('/api/group_c/question/add/' + newQuestion.title).success(function (data) {
      //     console.log(data);
      // });
      //   for (j = 0; j < answers.length; j++){  
      //     $http.get('/api/group_c/answer/add/' + (j+1) + '/' + answers[j].ans).success(function (data) {
      //       console.log(data);
      //       });
      // }}
    }
      else {
        $scope.flag = 1;
        $scope.errors = 'Please enter a question title';
        $timeout(function () {
          $scope.flag = 0;
        }, 3000);
      }     
  
    };

    // Add answer is for adding more answers to the question. FRONT-END ONLY
    $scope.addAnswer = function($event, answers) {
      if (answers.length < 5) {
        answers.push( {right: '', ans:''});
      }
      else {
        $scope.flag = 1;
        $scope.errors = 'You cannot add more than 5 answers.';
        $timeout(function () {
          $scope.flag = 0;
        }, 3000);        
       }
      $event.preventDefault();
    };

    $scope.removeAnswer = function(answers, answer) {
      var index = answers.indexOf(answer);
      if (answers.length > 2) {
        answers.splice(index, 1);
      }
      else {
        $scope.flag = 1;
        $scope.errors = 'You cannot have less than 2 answers.';
        $timeout(function () {
          $scope.flag = 0;
        }, 3000);        
       }
    };

    $scope.removeQuestion = function(question) {
      var index = $scope.questions.indexOf(question);
      $scope.questions.splice(index, 1);
      $scope.errors = ''
    }; 

    $scope.submitQuestions = function($event) {
      if ($scope.questions.length > 0) {
        // Backend
        var submission = $scope.questions;
        //var temp = $scope.qindex[0].qid;
        for (i = 0; i < submission.length; i++) {
          url = '/api/group_c/data/add/' + submission[i].title;
          for (j = 0; j < submission[i].answer.length; j++)
            url = url + '/' + submission[i].answer[j].ans;
          $http.get(url).success(function (data) {
            console.log(data);
            //alert(url);
          });
        }
        
        
        /* ****PLEASE READ******
           Submission is an object that contains all questions that have been created in
           the Add Questions tab, an example of this object is as follows:
           [
              {
                  "title": "Test Poll 1",
                  "answer": [
                      {
                          "right": false,
                          "ans": "Ans1",
                          "$$hashKey": "010"
                      },
                      {
                          "right": false,
                          "ans": "Ans2",
                          "$$hashKey": "011"
                      }
                  ],
                  "activated": false,
                  "$$hashKey": "01M"
              },
              {
                  "title": "Test Poll 2",
                  "answer": [
                      {
                          "right": true,
                          "ans": "Ans Correct",
                          "$$hashKey": "01O"
                      },
                      {
                          "right": false,
                          "ans": "Ans 3",
                          "$$hashKey": "01P"
                      },
                      {
                          "right": "",
                          "ans": "Ans 2",
                          "$$hashKey": "01U"
                      },
                      {
                          "right": "",
                          "ans": "Ans 1",
                          "$$hashKey": "01W"
                      }
                  ],
                  "activated": false,
                  "$$hashKey": "01Y"
              },
              {
                  "title": "Test Question",
                  "answer": [
                      {
                          "right": false,
                          "ans": "Rawr",
                          "$$hashKey": "020"
                      },
                      {
                          "right": true,
                          "ans": "123 correct",
                          "$$hashKey": "021"
                      },
                      {
                          "right": true,
                          "ans": "Double correct",
                          "$$hashKey": "028"
                      }
                  ],
                  "activated": false,
                  "$$hashKey": "02A"
              }
           ]
        What you need to do is call the API from here; something like this
          
          $http.get('/api/group_c/question/add/' + submission).success( function (data) {
            console.log(data);
            console.log("http request to " + '/api/group_c/question/add/' + submission + " succeed");
            $scope.retrievedQuestions = data;
          });
        
        Then parse the object that is sent and put it in respective tables etc. */

        // this is the code used to display success. put it in the API call if data returns success
        $scope.sflag = 1;
        $scope.success = "Questions submitted successfully!";
        $timeout(function () {
          $scope.sflag = 0;
        }, 3000);

        // otherwise put this code instead if there are errors in the API call
        // $scope.flag = 1;
        // $scope.errors = "There was a problem with the server. Please try again";
        // $timeout(function () {
        //   $scope.flag = 0;
        // }, 3000);
        //

        $scope.questions = [];
      }
      $event.preventDefault();
    };

    /* Front end presentation code, not needed
    $scope.pushAllQuestions = function(submission) {
       for (i = 0; i < submission.length; i++) { 
         $scope.retrievedQuestions.push(submission[i]);
         // Connect to the back-end
         $http.get('/api/group_c/question/add/' + questionTitle).success( function (data) {
           console.log(data);
           console.log("http request to " + '/api/group_c/question/add/' + questionTitle + " succeed");
           $scope.retrievedQuestions = data;
         });
         var newQuestion = angular.copy(submission[i]);
         $scope.displayedQuestions.push(newQuestion);
       }
       console.log($scope.retrievedQuestions);
       console.log($scope.displayedQuestions);
     };*/

    // Backend should return the same object as 'submission' or something similar.
    // This way we can parse it the exact same way in front end.

    $scope.retrieveQuestions = function() {
        $http.get('/api/group_c/data/get/').success( function (data) {
          console.log(data);
          console.log("http request to " + '/api/group_c/data/get/' + " succeed");
          $scope.retrievedQuestions = data;
        });
    }

    $scope.getIndex = function()
    {
        $http.get('/api/group_c/question/getcurrentindex').success( function (data) {
            console.log(data);
            console.log("http request to " + '/api/group_c/question/add/' + " succeed");
            return data[0].qid;
            });
    }

  });

  this.directive('ngEnter', function () {
      return function (scope, element, attrs) {
          element.bind("keydown keypress", function (event) {
              if(event.which === 13) {
                  scope.$apply(function (){
                      scope.$eval(attrs.ngEnter);
                  });

                  event.preventDefault();
              }
          });
      };
  });

  this.directive('showtab',
    function () {
        return {
            link: function (scope, element, attrs) {
                element.click(function(e) {
                    e.preventDefault();
                    $(element).tab('show');
                });
            }
        };
    });

}).call(angular.module('controllers'));