(function() {

  this.controller('GroupECtrl', function($scope, course, GroupE, $timeout) {

  /**
   * The current course is now passed to you via `course`.
   * When you are making your API calls, make sure to pass `course.id` to the
   * service function so that you can put the currrent course ID in your query
   * param.
   *
   * For example, instead of `.getStats()` you'd be able to do:
   * `.getStats(course.id)`.
   *
   */
  $scope.course = course;

    $scope.counter = 600;

    $scope.viewAttendance = 'View Attendance';
    $scope.startAttendance = "Initiate Attendance";
    $scope.kenny = "Kenny Chetal";
    $scope.startAttendanceResults = "Check In Status:";
    $scope.totalAttendance = 0;
    $scope.pin = "not set";

    $scope.tableData = [{ class_id:"null", id:"null", status:"null", student_id:"null", timestamp:"null"}];
    

    $scope.doTimer = function(){
       
        $scope.onTimeOut = function(){
            $scope.counter = $scope.counter -1;
            //basically canncels the timer when appropriate
            if($scope.counter==0 || $scope.counter == "0" || $scope.counter<0){
                $timeout.cancel($mytimeout);
            }
             mytimeout = $timeout($scope.onTimeOut,1000);
            //$scope.$apply();
        }
            var mytimeout = $timeout($scope.onTimeOut,1000);
            console.log($scope.counter);
            
    }

    GroupE.getAttendance().then(function(data) {
    	for(i = 0; i < data.length; i++){
			$scope.tableData[i] = data[i];

    	}
    	
    });


    $scope.getTimeStap = function(){

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 

        if(mm<10) {
            mm='0'+mm
        } 

        today = mm+'-'+dd+'-'+yyyy;
        return today;  
    }


    
    $scope.doClick = function() {

            //insert logic for checking whether there is a time today inserted...if yes update the row....


	    GroupE.setAttendance($scope.getTimeStap()).then(function(data) {
	    	if(data.class_id){ //if it returns a classid then data was inserted....
	    		$scope.startAttendanceResults="Check In Status: Started";
                $scope.pin = data.code;
	    	}
	    	
	    });

        $scope.doTimer();
     }

     $scope.getStats = function(){
        GroupE.getStats().then(function(data) {
           // var newdata = JSON.parse(JSON.parse(data).data));
           // console.log(newdata);
           console.log(JSON.parse(data));
           data = JSON.parse(data);
           data = JSON.parse(data);
           console.log(data);
           $scope.totalAttendance = data.length;
            
        });
     }

     //$scope.getStats();

     $scope.testAlert = function(event){
        //setup variables
        element = event.srcElement;
        param = element.className;
        id = param;
        tableRow = element.parentNode.parentNode;
        console.log(element.innerHTML);
        studentId=tableRow.children[1].innerHTML;
        classId = tableRow.children[2].innerHTML;
        timeStamp = tableRow.children[3].innerHTML;
        status = tableRow.children[4].innerHTML;
        tableContainer = element.parentNode.parentNode.parentNode.parentNode.parentNode;

        if(element.innerHTML=="Edit"){
            element.innerHTML="Save";
            tableRow.children[4].innerHTML='<input type="text" value="' +  status + '"' + '>';
            inputBox = tableRow.children[4].children[0];
            value = inputBox.value;
        }

        else if(element.innerHTML=="Save"){
            inputBox = tableRow.children[4].children[0];
            value = inputBox.value;
            tableRow.children[4].innerHTML=value;
            element.innerHTML="Edit";
            status = value;
            //do magic
            GroupE.changeStatus(id,studentId,classId,timeStamp,status).then(function(data) {
                    GroupE.getAttendance().then(function(data) {
                        for(i = 0; i < data.length; i++){
                            $scope.tableData[i] = data[i];

                        }
                        $scope.getStats();
                        
                    });
                
            });
        }
     }


  });

}).call(angular.module('controllers'));
