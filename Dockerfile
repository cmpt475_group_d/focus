FROM ubuntu:14.04

MAINTAINER Martin Pelikan <mpelikan@sfu.ca>

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install python3-dev python3-pip nginx curl vim mysql-server postfix
RUN pip3 install uwsgi pymysql

# Add requirements.txt separately so installing requirements can be
# independently cached.
ADD /requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

ADD / /focus

ADD /postfix.cf /etc/postfix/main.cf

ENV DJANGO_SETTINGS_MODULE focus.prod_settings

EXPOSE 80

CMD service rsyslog start && \
    service mysql start && \
    echo "create database focus" | mysql -u root && \
    python3 /focus/focus/manage.py syncdb --noinput && \
    service postfix start && \
    nginx -c /focus/nginx.conf && \
    uwsgi -d --ini /focus/uwsgi.ini && \
    tail -f /var/log/focus.log
