'''
File: urls.py
Author: Erick Costigan
Description: Creates routes for accessing the API functionality
Date Last Modified: July 19, 2014
'''

from django.conf.urls import patterns, url, include
from rest_framework import routers
from group_e import views
from rest_framework.urlpatterns import format_suffix_patterns


router = routers.DefaultRouter()
router.register(r'Attendance_Check', views.Attendance_Check_ViewAll)
router.register(r'Attendance', views.Attendance_ViewSet)
router.register(r'Server_Time', views.Server_Time_ViewSet)
#router.register(r'MyRESTView', views.MyRESTView, 'MyRESTView')
filter_fields = ('code')

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^Attendance_Check/(?P<pk>[0-9]+)/$', views.Attendance_Check_ViewSet.as_view()),
    url(r'^Attendance/(?P<pk>[0-9]+)/$', views.Attendance_ViewSet.as_view()),
    url(r'^Server_Time/(?P<pk>[0-9]+)/$', views.Server_Time_ViewSet.as_view()),
    url(r'^Stats/', views.Stats.as_view()),
    url(r'^Attendance_Populate/', views.Attendance_Populate.as_view()),
)
