'''
File: serializers.py
Author: Erick Costigan
Description: Serializer creation to handle incomming and outgoing requests to the API
Date Last Modified: July 6, 2014
'''

from group_e.models import Attendance_Check, Attendance,Server_Time
from rest_framework import serializers
from django.utils import timezone

class Attendance_Check_Serializer(serializers.ModelSerializer):
    call_time = serializers.SerializerMethodField('get_time')
    class Meta:
    	model = Attendance_Check
    def get_time(self, obj):
        return timezone.now()

class Attendance_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
    	model = Attendance
    	fields = ('id','student_id','class_id', 'timestamp', 'status')

class Server_Time_Serializer(serializers.ModelSerializer):
    time = serializers.SerializerMethodField('get_time')
    class Meta:
        model = Server_Time

    def get_time(self, obj):
        return timezone.now()
