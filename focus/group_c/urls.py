from django.conf import urls

from group_c import views

urlpatterns = urls.patterns(
    '',
    urls.url(r'^data/get/?$', views.AnswerManager.getData),
	urls.url(r'^data/add/(?P<qcontent>[\w\W\ ]+)/(?P<ans1>[\w\W\ ]+)/(?P<ans2>[\w\W\ ]+)/(?P<ans3>[\w\W\ ]+)/(?P<ans4>[\w\W\ ]+)/(?P<ans5>[\w\W\ ]+)/?$', views.AnswerManager.addData5),
	urls.url(r'^data/add/(?P<qcontent>[\w\W\ ]+)/(?P<ans1>[\w\W\ ]+)/(?P<ans2>[\w\W\ ]+)/(?P<ans3>[\w\W\ ]+)/(?P<ans4>[\w\W\ ]+)/?$', views.AnswerManager.addData4),
	urls.url(r'^data/add/(?P<qcontent>[\w\W\ ]+)/(?P<ans1>[\w\W\ ]+)/(?P<ans2>[\w\W\ ]+)/(?P<ans3>[\w\W\ ]+)/?$', views.AnswerManager.addData3),
    urls.url(r'^data/add/(?P<qcontent>[\w\W\ ]+)/(?P<ans1>[\w\ ]+)/(?P<ans2>[\w\ ]+)/?$', views.AnswerManager.addData2),
    urls.url(r'^question/get/?$', views.QuestionManager.getQuestion),
    urls.url(r'^question/remove/(?P<id>\d+)/?$', views.QuestionManager.deleteQuestion),
    urls.url(r'^question/activate/(?P<qid>\d+)/?$', views.QuestionManager.activateQuestion),
    urls.url(r'^question/deactivate/(?P<qid>\d+)/?$', views.QuestionManager.deactivateQuestion),
    urls.url(r'^question/add/(?P<content>[\w\W\ ]+)/?$', views.QuestionManager.addQuestion),
    urls.url(r'^question/modify/(?P<qid>\d+)/(?P<content>[\w\ ]+)/?$', views.QuestionManager.modifyQuestion),
    urls.url(r'^answer/get/?$', views.AnswerManager.getAnswer),
    urls.url(r'^answer/getspecificanswer/(?P<qid>\d+)/?$', views.AnswerManager.getSpecificAnswer),
    urls.url(r'^answer/add/(?P<number>\d+)/(?P<content>[\w\ ]+)/?$', views.AnswerManager.addAnswer),
)
