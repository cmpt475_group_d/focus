from django.db import models
import datetime

from django.utils.timezone import utc

now = datetime.datetime.utcnow().replace(tzinfo=utc)

# Create your models here.

class Question(models.Model):
	content = models.CharField(max_length = 255, blank = False, unique = True)
	creation_date = models.DateTimeField(auto_now_add = True, blank = False)
	activated = models.BooleanField(default = False)
	# objects = QuestionManager()

class Answer(models.Model):
	#question = models.ForeignKey('Question')
	qid = models.IntegerField(blank = False)
	number = models.PositiveSmallIntegerField(blank = False)
	content = models.CharField(max_length = 255, blank = False)
	# objects = AnswerManager()

class Submission(models.Model):
	#student = models.ForeignKey('auth.user') # expect an email or name or student ID here.
	student = models.CharField(max_length = 255, blank = False)
	#question = models.ForeignKey('Question')
	qid = models.IntegerField(blank = False)
	#answer = models.ForeignKey('Answer')
	aid = models.PositiveSmallIntegerField(blank = False)
	#value = models.PositiveSmallIntegerField()
	timestamp = models.DateTimeField(auto_now_add = True, blank = False)



