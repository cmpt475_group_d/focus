from rest_framework import serializers

from .models import *


class QuizSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quiz
        fields = ('id', 'quizName', 'creator', 'quizWorth', 'timestamp', 'updated', 'dueDay')


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ('id', 'quiz', 'qNumber', 'qType', 'qWorth', 'qText')


class PossibleAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = PossibleAnswer
        fields = ('id', 'question', 'answerText', 'correctAnswer')


class StudentQuizSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentQuiz
        fields = ('id', 'quiz', 'student', 'quizDueTime', 'score', 'completed')


class StudentAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentAnswer
        fields = ('id', 'studentQuiz', 'answerText')