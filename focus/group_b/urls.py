#from django.conf import urls
from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from group_b import views

'''
urlpatterns = urls.patterns(
    'group_b.views',
    url(r'^quiz/$', 'quiz_list'),
    url(r'^quiz/(?P<pk>[0-9]+)/$', 'quiz_detail'),
)
'''

#class based view
urlpatterns = patterns(
    '',
    url(r'^quiz/$', views.QuizList.as_view()),
    url(r'^quiz/(?P<pk>[0-9]+)/$', views.QuizDetail.as_view()),

    url(r'^question/$', views.QuestionList.as_view()),
    url(r'^question/(?P<pk>[0-9]+)/$', views.QuestionDetail.as_view()),

    url(r'^possibleAnswer/$', views.PossibleAnswerList.as_view()),
    url(r'^possibleAnswer/(?P<pk>[0-9]+)/$', views.PossibleAnswerDetail.as_view()),

    url(r'^studentQuiz/$', views.StudentQuizList.as_view()),
    url(r'^studentQuiz/(?P<pk>[0-9]+)/$', views.StudentQuizDetail.as_view()),

    url(r'^studentAnswer/$', views.StudentAnswerList.as_view()),
    url(r'^studentAnswer/(?P<pk>[0-9]+)/$', views.StudentAnswerDetail.as_view()),
)

urlpatterns = format_suffix_patterns(urlpatterns)

