"""Account management URL configuration.

urls.py
Worked on by: Tom Dryer (Team 1, Group A)
"""

from django.conf import urls
from rest_framework import routers

from accounts import views


urlpatterns = urls.patterns(
    '',
    urls.url(r'^courses/?$', views.CoursesView.as_view()),
    urls.url(r'^courses/(?P<course_id>\d+)/?$', views.CourseView.as_view()),
    urls.url(r'^session/?$', views.SessionView.as_view()),
    urls.url(r'^instructors/?$', views.InstructorView.as_view()),
)
